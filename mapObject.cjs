const testObject = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham'
}; // use this object to test your functions

// Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
// http://underscorejs.org/#mapObject

function cb (item, obj) {
    return obj[item] + 2;
}

function mapObject(obj, cb) {
    const result = {};
    for (let item in obj) {
        result[item] = cb(item, obj);
    }
    return result;
}

// console.log(mapObject(testObject, cb));

module.exports = mapObject;
