// Retrieve all the names of the object's properties.
// Return the keys as strings in an array.
// Based on http://underscorejs.org/#keys

const testObject = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham'
}; // use this object to test your functions

function keys(obj) {
    const keysInStrings = [];
    for (let item in obj) {
        keysInStrings.push(item);
    }
    return keysInStrings
}

// console.log(keys(testObject));
// console.log(Object.keys(testObject));

module.exports = keys;