
const defaults = require("../defaults.cjs");

const testObject = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham',
    nickName: undefined
}; 

const defaultProps = {
    nickName: 'Bat Man' 
};

test("Testing defaults", () => {
    expect(defaults(testObject, defaultProps)).toStrictEqual({ name: 'Bruce Wayne', age: 36, location: 'Gotham', nickName: 'Bat Man' })
});