
const {mapObject, cb} = require("../mapObject.cjs");

const testObject = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham'
}; 

test("Testing mapObject", () => {
    expect(mapObject(testObject, cb)).toStrictEqual({ name: 'Bruce Wayne2', age: 38, location: 'Gotham2' })
});