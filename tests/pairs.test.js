
const pairs = require("../pairs.cjs");

const testObject = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham'
}; 

test("Testing pairs", () => {
    expect(pairs(testObject)).toStrictEqual(Object.entries(testObject))
});