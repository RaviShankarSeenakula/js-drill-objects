
const invert = require("../invert.cjs");

const testObject = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham'
}; 

test("Testing invert", () => {
    expect(invert(testObject)).toStrictEqual({ '36': 'age', 'Bruce Wayne': 'name', Gotham: 'location' })
});