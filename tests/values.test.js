
const values = require("../values.cjs");

const testObject = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham'
}; 

test("Testing values", () => {
    expect(values(testObject)).toStrictEqual(Object.values(testObject))
});