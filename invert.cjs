// Returns a copy of the object where the keys have become the values and the values the keys.
// Assume that all of the object's values will be unique and string serializable.
// http://underscorejs.org/#invert

const testObject = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham'
}; // use this object to test your functions


function invert(obj) {
    const result = {};
    for (let item in obj) {
        result[obj[item]] = item;
    }
    return result;
}

// console.log(invert(testObject));

module.exports = invert;