// Fill in undefined properties that match properties on the `defaultProps` parameter object.
// Return `obj`.
// http://underscorejs.org/#defaults

const testObject = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham',
    nickName: undefined
}; // use this object to test your functions

const defaultProps = {
    nickName: 'Bat Man' 
};

function defaults(obj, defaultProps) {
    for (let item in obj) {
        if (obj[item] == undefined) {
            obj[item] = defaultProps[item];
        }
    }
    return obj;
}

// console.log(defaults(testObject, defaultProps));

module.exports = defaults;