
// Convert an object into a list of [key, value] pairs.
// http://underscorejs.org/#pairs

const testObject = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham'
}; // use this object to test your functions


function pairs(obj) {
    const result = [];
    for (let item in obj) {
        result.push([item , obj[item]]);
    }
    return result;
}

// console.log(pairs(testObject));
// console.log(Object.entries(testObject));

module.exports = pairs;