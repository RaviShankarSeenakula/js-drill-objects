
    // Return all of the values of the object's own properties.
    // Ignore functions
    // http://underscorejs.org/#values

const testObject = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham'
}; // use this object to test your functions

function values(obj) {
    const valuesOfObject = [];
    for (let item in obj) {
        valuesOfObject.push(obj[item]);
    }
    return valuesOfObject;
}

// console.log(values(testObject));
// console.log(Object.values(testObject));

module.exports = values;